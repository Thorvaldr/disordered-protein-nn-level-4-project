import torch
from torch import nn
import torch.nn.functional as F
import torchvision.models as models
import numpy as np
import math


#helper function for padding tensors in batch to be of even length
def fill_input_seq2(seq):
    diff = 1000 - len(seq)
    first = math.floor(diff/2)
    template = []
    for i in range(first):
        template.append(0)
    for item in seq:
        template.append(item)
        
    while len(template) < 1000:
        template.append(0)
        
    return template

def subSeq(sequence):
    sub_sequences = []
    for i in range(len(sequence)-14):
        sub_sequences.append(sequence[i:i+15])
    return sub_sequences

def pad_endings(sequence):
    base = []
    for i in range(7):
        base.append(0)
    for letter in sequence:
        base.append(letter)

    for i in range(7):
        base.append(0)
    return base

def pre_processing(sequence):

    residue_encoding = {'A':0, 'C':1, 'D':2, 'E':3, 'F':4, 'G':5, 'H':6, 'I':7, 'K':8, 'L':9, 'M':10, 
                    'N':11, 'P':12, 'Q':13, 'R':14, 'S':15, 'T':16, 'V':17, 'W':18, 'Y':19}

    seq_encoding = {'A':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6, 'H':7, 'I':8, 'K':9, 'L':10, 'M':11, 
                    'N':12, 'P':13, 'Q':14, 'R':15, 'S':16, 'T':17, 'V':18, 'W':19, 'Y':20}

    seq = sequence
    encoded_sub_sequence = []
    encoded_sequence = []
    nump_encoded = np.zeros((15,20))
    
    #encoding for residue
    for char in seq:
        encoded_sequence.append(residue_encoding[char])

    #encoded_sequence = pad_endings(encoded_sequence)
    
    all_subseqs = subSeq(encoded_sequence)

    #encoding for sequence
    encoded_sequence = []
    for char in seq:
        encoded_sequence.append(seq_encoding[char])

    all_onehot = []
    for subseq in all_subseqs:
        onehot_encoded = []
        for residue in subseq:
            letter = [0 for _ in range(20)]
            letter[residue] = 1
            onehot_encoded.append(letter)
        
        for i in range(0,15):
            nump_encoded[i,:] = onehot_encoded[i]
            
        onehot_encoded = [item for sublist in onehot_encoded for item in sublist]

            
        onehot_encoded = torch.tensor(onehot_encoded)
        all_onehot.append(onehot_encoded)

    encoded_sequence = fill_input_seq2(encoded_sequence)
    encoded_sequence = torch.tensor(encoded_sequence)
    
    return encoded_sequence, all_onehot

#CNN 
class Seq_model(nn.Module):
    def __init__(self):
        
        super(Seq_model, self).__init__()
        
        
        self.kernel1 = 3
        self.kernel2 = 4
        self.kernel3 = 5
        
        
        self.fmaps1 = 20
        self.fmaps2 = 20
        self.fmaps3 = 20
        
        self.stride = 1
        
        self.out_size = 20
        
        #self.dropout = nn.Dropout(0.5)
        
        self.conv1 = nn.Conv1d(1,self.fmaps1,self.kernel1)
        self.conv2 = nn.Conv1d(20,self.fmaps2,self.kernel2)
        self.conv3 = nn.Conv1d(20,self.fmaps3,self.kernel3)
        
        # Max pooling layers definition
        self.pool1 = nn.MaxPool1d(self.kernel1, self.stride)
        self.pool2 = nn.MaxPool1d(self.kernel2, self.stride)
        self.pool3 = nn.MaxPool1d(self.kernel3, self.stride)
        
        #initial_output = (((1000 - self.kernel1)/self.stride + 1)/self.kernel1 
                          #+ ((1000 - self.kernel2)/self.stride + 1)/self.kernel2
                          #+ ((1000 - self.kernel3)/self.stride + 1)/self.kernel3) # self.kernelx devisions is because of pooling
        
        #initial_output = self.fc_in_calc()
        self.fc1 = nn.Linear(19640,1000)
        self.fc2 = nn.Linear(1000,1)
        
    def forward(self, x):
        
        x = x.unsqueeze(1).unsqueeze(1).view(1,1,1000)
        
        # Convolution layer 1 is applied
        x = self.conv1(x)
        x = torch.relu(x)
        x = self.pool1(x)
        
        # Convolution layer 2 is applied
        x = self.conv2(x)
        x = torch.relu((x))
        x = self.pool2(x)

        # Convolution layer 3 is applied
        x = self.conv3(x)
        x = torch.relu(x)
        x = self.pool3(x)
        
        #print(x.shape[0])
        x = x.view(x.shape[0],19640)
        #print(x.shape)
        # The final convolution output is passed through a fully connected layer
        x = self.fc1(x)
        x = torch.relu(x)
        x = self.fc2(x)
        x = torch.sigmoid(x)
        #no need for second activation function because it is applied with loss function

        
        return x
    
    
    def train_step(self, batch):
        # Load batch
        labels = batch[0].long()
        sequences = batch[1].float()
        
        
        # Generate predictions
        output = self(sequences)#[1].float()
        
        #print(output.shape)
        labels = labels.unsqueeze(1)
        #print(output)
        
        # Calculate loss
        loss = F.binary_cross_entropy(output.float(), labels.float())
        return loss
    
    def validation_step(self, batch):
        # Load batch
        labels = batch[0].long()
        sequences = batch[1].float()

        # Generate predictions
        output = self(sequences)#[1].float()
    
    def get_predictions(self, batch):
        # Load batch
        #labels = batch[0].long().unsqueeze(1)#.to(self.device)
        sequences = batch.float()#.to(self.device)

        # Generate predictions
        output = self(sequences) 
        
        # Calculate loss
        #loss = F.binary_cross_entropy(output.float(), labels.float())

        # Calculate accuracy
        #acc = self.accuracy_function(output, labels)
        
        return output



#Basic pytorch feed forward neural network 
class Residue_model(nn.Module):

    def __init__(self, dimension=20):
        super(Residue_model, self).__init__()
        
        
        self.hidden_size = dimension
        
        
        self.num_layers = 2
        
        self.dimension = dimension
        self.lstm = nn.LSTM(input_size=300,
                            hidden_size=dimension,
                            num_layers=self.num_layers,
                            batch_first=True,
                            bidirectional=True)
        
        #self.drop = nn.Dropout(p=0.5)

        self.fc = nn.Linear(2*dimension, 1)

    def forward(self, seq, seq_len=300):
        seq = seq.unsqueeze(1)
        print(seq)
        seq = seq.view(1,300)

        h0 = torch.zeros(self.num_layers*2, seq.size(0),self.hidden_size)
        c0 = torch.zeros(self.num_layers*2, seq.size(0),self.hidden_size)
        
        #print(seq.shape)
        seq = seq.unsqueeze(1)
        
        #seq = seq.transpose(1, 2)
        #print(seq.shape)
        x, (hidden_out, cell_out) = self.lstm(seq, (h0, c0))
        #print(x.shape)
        
        #take final hidden states of both lstms and concat them
        hidden_out = torch.cat((hidden_out[-2,:,:], hidden_out[-1,:,:]), dim = 1)
        
        #text_fea = self.drop(out_reduced)

        hidden_out = self.fc(hidden_out)
        hidden_out = torch.squeeze(hidden_out, 1)
        hidden_out = torch.sigmoid(hidden_out)

        return hidden_out
    
    
    def train_step(self, batch):
        # Load batch
        labels = batch[0].long()
        sequences = batch[1].float()
        
        
        # Generate predictions
        output = self(sequences)#[1].float()
        
        #print(output.shape)
        #labels = labels.unsqueeze(1)
        #print(output)
        
        # Calculate loss
        loss = F.binary_cross_entropy(output.float(), labels.float())
        return loss
    
    def get_predictions(self, batch):
        # Load batch
        #labels = batch[0].long().unsqueeze(1)#.to(self.device).long()
        sequences = batch.float()#.to(self.device)

        # Generate predictions
        output = self(sequences) 
        
        # Calculate loss
        #loss = F.binary_cross_entropy(output.float(), labels.float())

        # Calculate accuracy
        #acc = self.accuracy_function(output.float(), labels.float())
        
        return output
        
        
from django.urls import path
from IDPPred import views

app_name = 'IDPPred'
urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('about', views.about, name='about'),
    path('help', views.help, name='help'),
    path('sources', views.sources, name='sources'),
]

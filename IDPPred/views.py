from django.shortcuts import render
from django.http import HttpResponse
from IDPPred.forms import SequenceForm
from IDPPred.pytorch_models import Seq_model, Residue_model, pre_processing
import numpy as np
import torch
from django.templatetags.static import static
import os


# Create your views here.
def index(request):
    context_dict = {}
    form = SequenceForm()
    if request.method == "POST":
        form = SequenceForm(request.POST)

        if form.is_valid():
            sequence = form.cleaned_data.get("Sequence")

            encoded_seq, all_onehot = pre_processing(sequence)

            seq_model = Seq_model()

            path = os.path.join(os.path.dirname(os.path.dirname(__file__)),'static/weights/PROT_CNNcustom_weights.pth')
            seq_model.load_state_dict(torch.load(path))

            seq_preds = seq_model.get_predictions(encoded_seq)

            res_model = Residue_model()

            path = os.path.join(os.path.dirname(os.path.dirname(__file__)),'static/weights/VEC_LSTMcustom_weights.pth')

            res_model.load_state_dict(torch.load(path))

            res_preds = []
            for subseq in all_onehot:
                print(subseq)
                res_pred = res_model.get_predictions(subseq)
                res_preds.append(res_pred)

            if (seq_preds.item() > 0.75):
                seq_data = {'sequence':sequence, 'class': 'Disordered', 'prediction':round(seq_preds.item()*100,2),'high':True}
            elif (seq_preds.item() > 0.5):
                seq_data = {'sequence':sequence, 'class': 'Disordered', 'prediction':round(seq_preds.item()*100,2),'med':True}
            elif (seq_preds.item() < 0.25):
                seq_data = {'sequence':sequence, 'class': 'Fully Ordered', 'prediction':round(seq_preds.item()*100,2), 'verylow':True}
            elif (seq_preds.item() < 0.5):
                seq_data = {'sequence':sequence, 'class': 'Fully Ordered', 'prediction':round(seq_preds.item()*100,2), 'low':True}

            print(len(sequence))
            print(len(res_preds))
            print(res_preds)
            res_counter = 0
            counter = 0
            letters = []
            for letter in sequence:
                if counter > 6 and counter < len(sequence)-7:
                    if (res_preds[res_counter].item() > 0.75):
                        letter_data = {'residue':letter, 'prediction': res_preds[res_counter].item(), 'high':True}
                    elif (res_preds[res_counter].item() > 0.5):
                        letter_data = {'residue':letter, 'prediction': res_preds[res_counter].item(), 'med':True}
                    elif (res_preds[res_counter].item() < 0.25):
                        letter_data = {'residue':letter, 'prediction': res_preds[res_counter].item(), 'verylow':True}
                    elif (res_preds[res_counter].item() < 0.5):
                        letter_data = {'residue':letter, 'prediction': res_preds[res_counter].item(), 'low':True}

                    letters.append(letter_data)
                    res_counter += 1
                else:
                    print(counter)
                    letter_data = {'residue':letter, 'prediction': 0, 'neutral':True}
                    letters.append(letter_data)

                counter += 1
            context_dict["letters"] = letters
            context_dict['sequence_data'] = seq_data
        
        return render(request, 'IDPPred/index.html', context=context_dict)

    context_dict["form"] = form
    context_dict["GET"] = "GET"
    return render(request, 'IDPPred/index.html', context=context_dict)

def about(request):
    context_dict = {}
    return render(request, 'IDPPred/about.html', context=context_dict)

def help(request):
    context_dict = {}
    return render(request, 'IDPPred/help.html', context=context_dict)

def sources(request):
    context_dict = {}
    return render(request, 'IDPPred/sources.html', context=context_dict)


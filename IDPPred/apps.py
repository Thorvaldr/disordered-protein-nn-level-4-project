from django.apps import AppConfig


class IdppredConfig(AppConfig):
    name = 'IDPPred'
